/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AddUserComponent } from './add-user.component.ts';

import { Router } from '@angular/router';
import { UserService } from '../../models/user/user.service';
import { CompanyService } from'../../models/company/company.service';
import { User } from '../../models/user/user';

let fixture;
let component;
let createComponent;

const subscribeStub = (data?) => () => ({
  subscribe: callback => callback(data)
});

const mock = {
  Router: {
    navigate: () => {}
  },
  UserService: {
    currentUser: 'stubCurrentUser',
    authorisationCheck: subscribeStub(),
    logout: () => {},
  },
  CompanyService: {
    currentCompany: {
      is_integrated: false,
    },
    createNewUser: subscribeStub(),
  }
};

describe('AddUserComponent', () => {

  beforeEach(() => {
    createComponent = () => {
      TestBed.configureTestingModule({
        declarations: [
          AddUserComponent
        ]
      });

      TestBed.overrideComponent(AddUserComponent, {
        set: {
          providers: [
            { provide: UserService, useValue: mock.UserService },
            { provide: CompanyService, useValue: mock.CompanyService },
            { provide: Router, useValue: mock.Router },
          ],
          template: '<div></div>'
        },
      });

      fixture = TestBed.createComponent(AddUserComponent);
      component = fixture.debugElement.componentInstance;
    };
  });

  it('should create the app and provide init values', async(() => {
    createComponent();

    expect(component).toBeTruthy();
    expect(component.spinner).toEqual(false);
  }));

  it('should set currentUser field from UserService after authorisation check', async(() => {
    spyOn(mock.UserService, 'authorisationCheck').and.callThrough();

    createComponent();

    expect(mock.UserService.authorisationCheck).toHaveBeenCalled();
    expect(component.currentUser).toEqual('stubCurrentUser');
  }));

  it('should set newUser field from UserService after authorisation check', () => {
    spyOn(User, 'createBlankUser').and.callThrough();

    createComponent();

    expect(User.createBlankUser).toHaveBeenCalled();
  });

  it('should add new user on addUser()', () => {
    spyOn(mock.CompanyService, 'createNewUser').and.callThrough();
    spyOn(mock.Router, 'navigate').and.callThrough();

    createComponent();
    component.newUser = 'stubNewUser';
    component.addUser();

    expect(mock.CompanyService.createNewUser).toHaveBeenCalledWith('stubNewUser');
    expect(mock.Router.navigate).toHaveBeenCalledWith(['users']);
  });

  it('should log out on signOut()', () => {
    spyOn(mock.UserService, 'logout').and.callThrough();

    createComponent();
    component.signOut();

    expect(mock.UserService.logout).toHaveBeenCalled();
  });

  it('should not redirect to users list if company is integrated with OA', () => {
    spyOn(mock.Router, 'navigate').and.callThrough();

    createComponent();
    component.ngOnInit();

    expect(mock.Router.navigate.calls.any()).toEqual(false);
  });

  it('should redirect to users list if company is integrated with OA', () => {
    spyOn(mock.Router, 'navigate').and.callThrough();

    mock.CompanyService.currentCompany.is_integrated = true;

    createComponent();
    component.ngOnInit();

    expect(mock.Router.navigate).toHaveBeenCalledWith(['users']);
  });
});
