/*
 User control panel
 */
import { Component, Inject } from '@angular/core';
import { UserService } from '../../models/user/user.service';
import { User } from '../../models/user/user';
import { CompanyService } from '../../models/company/company.service';
import { Animate } from '../../animations/animation.service';
import { Router } from '@angular/router';

@Component({
  animations: [Animate.states('pageFade')],
  selector: 'add-user',
  templateUrl: './add-user.html',
})
export class AddUserComponent {
  newUser = User.createBlankUser();
  currentUser: User;
  spinner = false;

  constructor(
    @Inject(UserService) private user: UserService,
    @Inject(CompanyService) private company: CompanyService,
    @Inject(Router) private router: Router
  ) {
    this.user.authorisationCheck().subscribe(() => {
      this.currentUser = this.user.currentUser;
    });
  }

  addUser() {
    this.spinner = true;
    this.company.createNewUser(this.newUser)
      .subscribe((users) => {
        this.spinner = false;
        this.router.navigate(['users']);
      });
  }

  signOut() {
    this.user.logout();
  }

  ngOnInit() {
    if (this.company.currentCompany.is_integrated) {
      this.router.navigate(['users']);
    }
  }
}
