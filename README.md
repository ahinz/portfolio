Explaning note

As I am not able to provide whole project code (due to legal reasons), I would like to offer you some pieces of code, that are relative isolated and easier to understand. However, some of them might need several comments, that I provide in this note.

**01.themeConfiguration.js** - this is Angular 1.x configuration unit, that is designed to support verbose branding in SPA by setting up Angular Material themes and palletes. There are several challenges here. First - Angular Material uses Material Design palettes to set-up elements colors. MD pallete provide shades of color - there are not verbose. F.e. there is no possibility to configure controlls color. You configure shade "300", "400" and "500" that are used in controls you need. This configuration takes branding configuration in readable format (f.e. { primaryColor: #ffbbff }) and parses it into SPA themes and palletes. Second challenge here was to make this code as FP as possible - this means maximum pure functions and point-free. This was the very start for team of having fun with FP as standard, thats why some parts seems to be a little bit ugly, and also thats why you can see two FP libraries - ramda and lodash/fp - it was playground to choose one of them

**02.editRequestParameter** - this is Angular 1.x component. It is not so interesting - just regular code sample. I chose this one just because it was written by me. You could mention here, that branches here don't use FP - thats because we discovered, that functional declaration of branch is not detected as branch by Istanbul coverage reporter. So for consistant unit test coverage monitoring we decided to avoid using functions that provide branching. 

**03.stubPromise.js** - well this one is fun. Code is a little bit messy, but I like it, because it represents my way of thinking. I wrote it about a year and a half ago in about 15 minutes. This is helper for unit testing. If you ever wrote several hundreds of unit tests for Angular 1.x, I bet you had several hours with incredible fun with imitating real things (like API calls, async actions etc.) to trigger some parts of your promise chain. The point here is, that mostly you just want to call some callback with some data passed, and not testing how angular $q service works. This small helper makes it deadly easy. In a very simple way - with very clear API you configure which callback of your chain you want to call and what data to pass. This small helper is used hundreds of times all over the project and saved hours of worktime for whole team.

**04.CheckFieldDialog.vue** - just a small example with Vue. Teams main project is currently migrating from Angular to VueJS - so I work currently very close with this framework

**05.mvc** - This is extention for Dojo Framework data-binding (if you want details - read long text below, otherwise just skip it) In brief - this is hell. 
I decided to write an extention, that will make data-binding easy and generic as possible.
This guy uses native Proxy and stores data in three conditions - pure data, Dojo mess and bounds for complex observers. This makes data-binding relative clear and provides several possibilities, that native implementation does not have (set with assign, observe several properties of object, return pure data, better syntax)

***How bad is Dojo Framework data-binding?*** 

First of all - why Dojo? Well this is realy tricky and needs some comments about product. Our project (APS Connect - https://www.ingrammicrocloud.com/aps-connect) in fact is a framework/IDE over Ingram Micro Cloud Platform (Is well known as Odin Automation and was rebranded as Cloud Blue). The reason of our product is to onboard vendors apps into IM infrastructure. This includes declarative frontend part, for which company designed its own frontend framework (APS Standrd - https://doc.apsstandard.org ) this framework is based on very old thing is called Dojo framework. It does lots of weird stuff, but now we have it as legacy. 
So now I can explain what is all this about. What you see in mvc.js file is actually a improvement for Dojo framework data-binding. Original data-binding looks something like this (very brief):
1. You declare observed object with dojo/getStateful
2. You set object property with method $set (direct assign won't work)
3. You get value and deal with it with "simple" structure: at(object, 'property').direction(at.from).transform({format: transformFuntion});
4. As a bonus1 - you are not able to watch several properties of object. Either one, or all.
5. As a bonus2 - as a result of observing you receive not clear JS data, but Dojo-types. This means Array that you receive won't be JS Array - it is Object enriched with Dojo methods, that partly override native JS Array methods by custom logic (most modern they just don't have). Also they are not in __proto__ but are set as Object properties, so hasOwnProperty wont work.

***That bad.***

**06.add-user** - just a small example, demonstrates that I also can Angular 2+ and Typescript. It was a small demonstration project. Basicaly we chose framework to move from A 1.x and tested Angular 2+. We chose Vue)

I guess this is it for now. If you would like to see more examples - just request from Jacek. Maybe some interesting things about e2e tests or some Python code, maybe ansible playbooks or ci configs. I just focused on JS part, but have a lot of different experience.
Thank you for your attention.
Best regards)















