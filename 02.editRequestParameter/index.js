import controller from './editRequestParameter';


export default function editRequestParameter(app) {
  app.component('editRequestParameter', {
    controller,
    bindings: {
      parameter: '<',
      onApprove: '&',
      onRemove: '&',
      onClose: '&',
      ngIf: '=',
    },
  });
}
