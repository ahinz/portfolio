import {
  complement,
  flatten,
  invoker,
  map,
  pathEq,
  pipe,
  prop,
  unless,
  values,
} from 'ramda';

import { patterns } from 'app/constants';


// eslint-disable-next-line dependencies/no-unresolved
import template from './editRequestParameter.pug?inline';

const touchEachErrorField = pipe(
  prop('$error'),
  values,
  flatten,
  map(invoker(0, '$setTouched')),
);

export default function editRequestParameter(
  $q,
  $mdDialog,
  themeService,
) {
  this.$onInit = () => {
    this.theme = themeService.theme;
    this.processing = false;
    this.confirmDelete = false;
    this.namePattern = patterns.WORD;
    this.alphanumericTextPattern = patterns.TEXT;

    $mdDialog
      .show({
        template,
        controller: Object.assign(($scope) => { this.dialogScope = $scope; }, { $inject: ['$scope'] }),
        controllerAs: '$ctrl',
        bindToController: true,
        locals: this,
        clickOutsideToClose: true,
        onRemoving: this.onCancel,
      });
  };

  this.onCancel = () => {
    this.ngIf = false;
  };

  this.apply = () => {
    this.processing = true;

    if (this.dialogScope.editParameterForm.$valid) {
      return this.onApprove()
        .then($mdDialog.cancel)
        .catch(unless(
          complement(pathEq)(['data', 'type'], 'duplicate_entry'),
          () => this.dialogScope.editParameterForm.name.$setValidity('duplicate-entry', false),
        ))
        .finally(() => {
          this.processing = false;
        });
    }

    touchEachErrorField(this.dialogScope.editParameterForm);

    return $q.when();
  };

  this.remove = () => this.onRemove()
    .then($mdDialog.cancel);

  this.cancel = () => {
    $mdDialog.hide();
    this.onCancel();
    this.onClose();
  };
}

editRequestParameter.$inject = [
  '$q',
  '$mdDialog',
  'themeService',
];
