import stubPromise from 'unit/helpers/stubPromise';

describe('editRequestParameter', () => {
  let $mdDialog;
  let ctrl;
  let bindings;

  beforeEach(angular.mock.module('sweeft-spa'));

  beforeEach(inject((
    $componentController,
    _$mdDialog_,
  ) => {
    $mdDialog = _$mdDialog_;

    bindings = {
      parameter: {},
      onApprove: () => {},
      onRemove: () => {},
      onClose: () => {},
      ngIf: true,
    };

    spyOn($mdDialog, 'show');
    spyOn($mdDialog, 'cancel');
    spyOn($mdDialog, 'hide');

    ctrl = $componentController('editRequestParameter', null, bindings);
  }));

  describe('call method #$onInit()', () => {
    it('sets dialog theme', () => {
      const mockThemeService = {
        theme: 'vendorTheme',
      };

      ctrl.$onInit();
      expect(ctrl.theme).toEqual(mockThemeService.theme);
    });
  });

  describe('call method #onCancel', () => {
    it('sets #ngIf to false', () => {
      ctrl.onCancel();
      expect(ctrl.ngIf).toBe(false);
    });
  });

  describe('call method #apply', () => {
    beforeEach(() => {
      ctrl.dialogScope = {
        editParameterForm: {
          $valid: true,
        },
      };
    });

    it('calls #onApprove when form is valid', () => {
      spyOn(ctrl, 'onApprove').and.callFake(() => stubPromise({}));
      ctrl.apply();
      expect(ctrl.onApprove).toHaveBeenCalled();
    });

    it('touches every invalid field if form is invalid', () => {
      ctrl.dialogScope = {
        editParameterForm: {
          $valid: false,
          $error: {
            required: [
              { $setTouched: jasmine.createSpy('$setTouched') },
            ],
          },
        },
      };

      ctrl.apply();
      expect(ctrl.dialogScope.editParameterForm.$error.required[0].$setTouched).toHaveBeenCalled();
    });
  });

  describe('call method #remove', () => {
    beforeEach(() => {
      spyOn(ctrl, 'onRemove').and.callFake(() => stubPromise({}));
      ctrl.remove();
    });

    it('calls #onRemove', () => {
      expect(ctrl.onRemove).toHaveBeenCalled();
    });

    it('closes dialog', () => {
      expect($mdDialog.cancel).toHaveBeenCalled();
    });
  });

  describe('call method #cancel', () => {
    beforeEach(() => {
      spyOn(ctrl, 'onCancel').and.callFake(() => {});
      spyOn(ctrl, 'onClose').and.callFake(() => {});
      ctrl.cancel();
    });

    it('calls #onCancel', () => {
      expect(ctrl.onCancel).toHaveBeenCalled();
    });

    it('calls #onClose', () => {
      expect(ctrl.onClose).toHaveBeenCalled();
    });

    it('closes dialog', () => {
      expect($mdDialog.hide).toHaveBeenCalled();
    });
  });
});
