import getStatefulDojo from 'dojox/mvc/getStateful';
import common from 'aps/common';
import at from 'dojox/mvc/at';

/**
 *  as() is a syntax wrapper on dojox/mvc/at binding function
 *
 *  How to use:
 *  1. as(Object, 'property') || as.both(Object, 'property') - creates boner-way data-binding with Object.property
 *
 *       for example:
 *       let myVar = as(Object, 'property');
 *       Object.property = false; // myVar === false
 *       myVar = true;            // Object.property === true;
 *
 *  2. as(Object, 'property', function) - the same thing, but bound data is transformed with "function",
 *       wich is a Function, that takes bound data as an argument and should return transformed value.
 *
 *       for example:
 *       let myVar = as(Object, 'property', (value) => !value);
 *       Object.property = false; // myVar === true;
 *       myVar = true;            // Object.property === false;
 *
 *  3. as.from(Object, 'property') - sets one-way recepient logic data-binding;
 *
 *       for example:
 *       let myVar = as.from(Object, 'property');
 *       Object.property = false; // myVar === false;
 *       myVar = true;            // Object.property === false;
 *
 *  4. as.to(Object, 'property') - sets one-way source logic data-binding;
 *
 *       for example:
 *       let myVar = as.to(Object, 'property');
 *       myVar = true;            // Object.property === true;
 *       Object.property = false; // myVar === true;
 */
function as(obj, prop, transform) {
  let binding = at(obj, prop);

  if (typeof transform === 'function') {
    binding = at(obj, prop).transform({format: val => transform(val)});
  }

  return binding;
}

as.from = (obj, prop, transform) => {
  let binding = at(obj, prop).direction(at.from);

  if (typeof transform === 'function') {
    binding = at(obj, prop).direction(at.from).transform({format: val => transform(val)});
  }

  return binding;
};

as.to = (obj, prop, transform) => {
  let binding = at(obj, prop).direction(at.to);

  if (typeof transform === 'function') {
    binding = at(obj, prop).direction(at.to).transform({format: val => transform(val)});
  }

  return binding;
};

as.both = (obj, prop, transform) => as(obj, prop, transform);

/**
 *
 *  Scope is an enchanced model concept, inspired by AngularJS scope functionality
 *  it is useful for:
 *  - more intuitive data-binding in widgets
 *  - sequenced data-binding
 *  - standartization of binding
 *
 */

// Helpers

const s4 = () => {
  return Math.floor((1 + Math.random()) * 0x10000)
    .toString(16)
    .substring(1);
};

const guid = () => s4() + s4() + '-' + s4() + '-' + s4() + '-' +
s4() + '-' + s4() + s4() + s4();

const isBasicType = (value) => {
  return typeof value === 'boolean'
    || typeof value === 'number'
    || typeof value === 'undefined'
    || (typeof value === 'object' && value === null)
    || typeof value === 'string';
};

// CLASS itself
class Scope {
  /** HOW IT WORKS:
    Each property of scope is an access point to data, stored in three conditions
      1. Pure data - as it is awaited in native workflow
      2. Stateful data - as it is required for Dojo workflow - provides tracking changes and communication with widgets infrastructure
      3. Bounds data - is also Dojo-stateful type, but combined for each package-watch - is stored separately and provides same events and functionality as Stateful data, but for group of properties

    When property is requested scope should return pure value of property (properties, when bound is requested)
    When property is set scope should provide 3 actions:
      1. Update pure data
      2. Update stateful data, which triggers updates of simple data in Dojo infrastractupe
      3. Update data in bounds, which triggers update of complex data in Dojo infrastructure
   */

  constructor(obj) {
    // Private Dojo Stateful model
    this.__stateful = getStateful({});
    // Pure data - is refreshed each time with stateful model -
    // used to return data as it is awaited - without Stateful and Proxy transformations
    // It is very important for keeping data consistent in workflow - to make it work with native methods, not thru Dojo overrides
    this.__pure = {};
    // Keeps for each property Array of bounds IDs, wich makes it easier to refresh connected bounds
    this.__map = {};
    // Storage of existing bounds
    // Bound is a sequence of observed properties.
    // In fact each one is one-level Stateful object, that is tracked with '*' as property
    this.__bounds = {};

    // Set all properties from raw object as scope fields
    for (const prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        this.$add(prop, obj[prop]);
      }
    }

    // This Proxy will be required in the future
    // 1. To filter direct get operations with scope properties
    //    Now when you try to get complex scope property (Object-like type) you will receive Proxy object of it
    // 2. To add new properties to Scope in more generic way - with simple scope.newProperty = value
    //    now it requires $add method
    return new Proxy(this, {
      get: (target, property) => target[property],
      set: (target, property, value) => {
        target[property] = value;

        return true;
      },
    });
  }

  // $set and $get and $add are very temporary things - they should move to scope declaration
  // In fact they are required mostly for Arrays. Objects work just fine.
    $get(prop) {
    return this.__pure[prop];
  }

  $set(prop, val) {
    if (typeof this[prop] !== 'undefined') {
      this.__updateProperty(prop, val);
    } else {
      this.$add(prop, val);
    }
  }

  // Public method to add properties to scope
  // Possibly will change to something more generic in the future
  $add(prop, val) {
    // Each property of scope is a proxy, that either
    // operates __stateful model thru getter-setter interface (when is simple type - string, number or boolean)
    // or is a Proxy object (when it is enything else Object-like)
    // this provides possibility to simplify model set and avoid possible errors
    if (prop) {
      this.__pure[prop] = val;
      this.__stateful.set(prop, val);

      if (isBasicType(this.__stateful[prop])) {
        // Properties with basic types are configured with simple getter-setter
        Object.defineProperty(this, prop, {
          get: () => this.__pure[prop],
          set: (value) => {
            this.__updateProperty(prop, value);
          },
        });

        // Initial set of property value
        this[prop] = val;
      } else {
        // Object-like types are configured with Proxys - to track there properties and methods more generic
        this[prop] = new Proxy(this.__stateful[prop], {
          get: (target, property) => target[property],
          set: (target, property, value) => {
            target[property] = value;
            this.__updateProperty(prop, target);

            return true;
          },
        });
      }

      // Creating properties collection of bounds ids
      this.__map[prop] = [];
    } else {
      console.warn(`SCOPE ERROR. Trying to add property with invalid name: ${prop} and value ${val}`);
    }
  }

  // Private method, used to update property values in existing bounds
  __updateValuesInBounds(prop) {
    if (this.__map && this.__map[prop]) {
      for (let i = 0; i < this.__map[prop].length; i++) {
        this.__bounds[this.__map[prop][i]].set(prop, this.__pure[prop]);
      }
    }
  }

  __updateProperty(prop, val) {
    this.__pure[prop] = val;
    this.__updateValuesInBounds(prop);
    this.__stateful.set(prop, val);
  }

  // Public method, used to observe values of properties or bounds (using overload)
  // For single properties:
  //
  //    scope.$watch('property_name'[, transform]);
  //      where 'property_name' is a property in scope to track
  //      transform is a function, that is used to transform returning value
  //      it takes actual property value and returns transformed one
  //      it is called each time when property is changed
  //
  // For bounds:
  //
  //    scope.$watch([property1, property2], transform);
  //      transform function in this case is called each time one of properties is changed
  //      it takes an object, where key is property name and value is it actual value

  $watch(prop, transform) {
    let _sourceObject = this.__stateful;
    let _trackedProperty = prop;
    let _prepareRawResult = () => this.__pure[prop];

    let _transformResult = val => val;

    if (transform) {
      _transformResult = transform;
    }

    if (Array.isArray(prop)) {
      const boundId = this.__watchBound(prop);
      _sourceObject = this.__bounds[boundId];
      _trackedProperty = '*';
      _prepareRawResult = () => this.__bounds[boundId].$get();
    }

    // Situation is not perfect here, because "at" (as) works as binding for Widgets only
    // Possibly either here, or in "as" wrapper we should standartize binding entry-point
    return as.from(_sourceObject, _trackedProperty, () => _transformResult(_prepareRawResult()));
  }

  // Private method for bound-watch logic
  __watchBound(bound) {
    // each bound is stored under its unic guid-like id
    const boundId = guid();
    const scope = this;

    // For now there is no need to create Bound as separate class because of it local usage
    // NB its "public" methods are relative pubhlic, because normally Bounds are used only inside the Scope
    const src = {
      // Updater is a property to trigger model change without touching observed properties
      __updater: 0,
      $touch() {
        this.set('__updater', this.__updater + 1);
      },
      // method filters all unused fields (f.e. Stateful fields and methods) to return pure result
      $get() {
        const res = {};

        for (const item in this) {
          if (this.hasOwnProperty(item) && bound.indexOf(item) > -1) {
            res[item] = scope.__pure[item];
          }
        }

        return res;
      },
    };

    for (let i = 0; i < bound.length; i++) {
      // This part deals with situation when trying to watch unexisted property
      // It might be common error propvided f.e. by misstype, so we don't want to fall here
      if (typeof this[bound[i]] === 'undefined') {
        console.warn(`SCOPE ERROR. Property 
          "${bound[i]}" is trying to be watched in 
          "${bound}" group-binding before it is declared.`);

        this.$add(bound[i], {});
      }

      this.__map[bound[i]].push(boundId);
      src[bound[i]] = this[bound[i]];
    }

    this.__bounds[boundId] = getStateful(src);

    return boundId;
  }

  // Public method to touch each existing bound
  // Current limitation for using bound watches requires call this after init() is complete
  // Otherwise initial values won't be set - this should be removed in the future
  touch() {
    for (const bound in this.__bounds) {
      if (this.__bounds.hasOwnProperty(bound)) {
        this.__bounds[bound].$touch();
      }
    }
  }
}

export default {
  getStateful,
  computed,
  Scope,
  as,
};
