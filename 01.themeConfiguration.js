import {
  get,
  set,
  getOr,
  curry,
  compose,
  isEmpty,
  isEqual,
  negate,
} from 'lodash/fp';

import { forEach } from 'lodash';

import {
  ifElse,
  isNil,
  map,
} from 'ramda';

import {
  call,
} from '../utils';

const getOrFrom = curry((obj, or, key) => getOr(or, key, obj));

const setIf = curry((cond, val, key, obj) => call(ifElse(
  () => cond,
  () => set(key, val, obj),
  () => obj,
)));

const setIfVal = curry((val, key, obj) => setIf(negate(isEmpty)(val), val, key, obj));

const hexa = (hex, opacity) => {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

  return result ?
    `rgba(${parseInt(result[1], 16)}, ${parseInt(result[2], 16)}, ${parseInt(result[3], 16)}, ${opacity})` :
    null;
};

const hexBrightness = (hex, delta) => compose(
  rgb => `rgba(${rgb[1]}, ${rgb[2]}, ${rgb[3]})`,
  map((v) => {
    const val = v + delta;

    if (val < 0) {
      return 0;
    } else if (val > 255) {
      return 255;
    }

    return val;
  }),
  map(v => parseInt(v, 16)),
  v => /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(v),
)(hex);

/** registerTheme - safe way to configure themes for angular material
 *
 *  Supported options:
 *  [Name]: [Default value] ([Used for])
 *
 *  palette: 'blue' (Basic palette for theme)
 *  primaryColor [REQUIRED]: palette[700] (Header toolbar, dialogs title sections etc.)
 *  backgroundColor: #ffffff (Body background color)
 *  containersColor: backgroundColor (Sidebar background)
 *  controlsColor: primaryColor (Controls in active state - switchers, checkboxes, inputs etc.)
 *  contrast: 'light' (Can also be 'dark'. If dark, header toolbar content etc. dark)
 * */
const registerTheme = curry(($mdThemingProvider, name, opts) => {
  const prepareOptions = (_opts) => {
    const result = _opts;

    if (isNil(_opts.controlsColor)) {
      result.controlsColor = hexBrightness(_opts.primaryColor, 50);
    }

    return result;
  };

  const options = prepareOptions(opts);
  const option = getOrFrom(options);

  // define theme
  const theme = $mdThemingProvider.theme(name);

  // fill palette config according to passed options object
  const paletteOptions = compose(
    setIf(isEqual(option('light', 'contrast'), 'dark'), ['50', '700', '800'], 'contrastDarkColors'),
    set('contrastDarkColors', ['50']),
    setIfVal(hexa(get('primaryColor', options), 0.2), 100),
    setIfVal(get('controlsColor', options), 500),
    setIfVal(get('controlsColor', options), 600),
    setIfVal(get('primaryColor', options), 700),
    setIfVal(get('primaryColor', options), 800),
    set(900, '#000000'),
    set(50, option('#ffffff', 'backgroundColor')),
    set('A100', option(option('#ffffff', 'backgroundColor'), 'containersColor')),
    set('A200', '#000000'),
    set('contrastDefaultColor', 'light'),
  );

  const themePalette = $mdThemingProvider.extendPalette(option('blue', 'palette'), paletteOptions({}));
  // register palette
  $mdThemingProvider.definePalette(`${name}Palette`, themePalette);

  // configure theme
  theme.primaryPalette(`${name}Palette`, {
    default: '600',
    'hue-1': '700',
    'hue-2': '50',
  });

  theme.backgroundPalette(`${name}Palette`, {
    default: '50',
  });

  theme.accentPalette(`${name}Palette`, {
    default: '600',
  });

  theme.warnPalette('red');
  theme.foregroundPalette['1'] = 'rgba(0,0,0,0.97)';
  theme.foregroundPalette['2'] = 'rgba(0,0,0,0.67)';
  theme.foregroundPalette['3'] = 'rgba(0,0,0,0.57)';

  $mdThemingProvider.alwaysWatchTheme(true);

  // Controls theme - there are some cross-overs in tones usage within themes.
  // That's why it is not possible to use one theme for all settings
  // define control theme
  const controlsTheme = $mdThemingProvider.theme(`${name}Controls`, 'default');

  // primary for controls like switchers, buttons etc.
  const controlsPaletteOptions = compose(
    setIf(isEqual(option('light', 'contrast'), 'dark'), ['700', '800'], 'contrastDarkColors'),
    setIfVal(get('controlsColor', options), 600),
    set('contrastDefaultColor', 'light'),
  );

  const themeControlsPalette = $mdThemingProvider.extendPalette(option('blue', 'palette'), controlsPaletteOptions({}));
  // register controls palette
  $mdThemingProvider.definePalette(`${name}ControlsPalette`, themeControlsPalette);

  controlsTheme.primaryPalette(`${name}ControlsPalette`, {
    default: '600',
  });

  controlsTheme.accentPalette(`${name}ControlsPalette`, {
    default: '600',
  });
});

export default function themeConfiguration(
  $mdThemingProvider,
  $injector,
) {
  const addTheme = registerTheme($mdThemingProvider);
  const branding = $injector.get('branding');

  forEach(branding.themes, (v, key) => {
    addTheme(`${key}Theme`, {
      primaryColor: v.theme.primary,
    });
  });

  $mdThemingProvider.generateThemesOnDemand(true);
}


themeConfiguration.$inject = [
  '$mdThemingProvider',
  '$injector',
];
